<?php

require_once "../../../controladores/clase.controlador.php";
require_once "../../../modelos/clase.modelo.php";

require_once "../../../controladores/maestro.Controlador.php";
require_once "../../../modelos/maestro.modelo.php";

require_once "../../../controladores/alumno.controlador.php";
require_once "../../../modelos/alumno.modelo.php";

require_once('tcpdf_include.php');

class MYPDF extends TCPDF{

	public function Header(){
		$this->SetFont('helvetica', 'B', 14);
 		$this->Cell(180,3,"IGLESIA BAUTISTA DIVINO MAESTRO",0,1,'C');
        $this->Cell(180,3,"ESCUELA DOMINICAL",0,1,'C');
        $this->Cell(180,3,"Formato de Asistencia Dominical",0,1,'C');
		$this->Image('images/LOGO4.png',160,5,30,20);           

	}
}

class imprimirFormato{

public $idClase;
public $anio;
//public $idMaestro;

public function traerFormato(){

$lclase = $this->idClase;
$ani = $this->anio;

$clase = ControladorClase::ctrMostrarClase($lclase);

$maestro = ControladorMaestro::ctrMostrarMaestro($lclase);

$listado = ControladorAlumno::ctrMostrarFormato($lclase, $ani);



$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetMargins(10,25,10,true);

$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);


$pdf->startPageGroup();

$pdf->AddPage();

// ---------------------------------------------------------

$bloque1 = <<<EOF

	<table>
		<tr>
			<td style="background-color:white; width:540px">
				
				<div style="font-size:12px; text-align:left; line-height:15px;">
					
					<br>
					<b>Clase: </b>$clase

					<br>
					<b>Maestro:</b> $maestro

					<br>
					<br>
					<b>1. Información de la sesion de clase (Encuentro)</b>
					<br>
					<b>1.1 Fecha en que se realizó la clase(dd/MM/aaaa):</b>___/___/_____
					<br>
					<b>1.2 Ofrenad recaudada C$:</b>__________
					<br>
				</div>

			</td>

		</tr>

		<tr>
			<td style="border: 2px solid #000; background-color:white; width:540px;">
				<div style="font-size:11px; text-align:left; line-height:15px;">
					<b>1.3 Observaciones:</b>
					<br>
					<br>
				</div>
			</td>
		</tr>

		<tr>
			<td style="background-color:white; width:540px">
				<div style="font-size:12px; text-align:left; line-height:15px;">
					<b>2. Listado de Asistencia</b>
				</div>			
			</td>

		</tr>
		<tr>
		
			<td style="border-bottom: 1px solid #666; background-color:white; width:540px"></td>

		</tr>

	</table>
EOF;
$pdf->writeHTML($bloque1, false, false, false, false, '');

// ---------------------------------------------------------
$bloque2 = <<<EOF

	<table style="font-size:12px; padding:2px 3px;">

		<tr>
		
		<td style="border: 1px solid #666; background-color:white; width:70px; text-align:left"><b>Codigo</b></td>
		<td style="border: 1px solid #666; background-color:white; width:150px; text-align:left"><b>Nombres</b></td>
		<td style="border: 1px solid #666; background-color:white; width:150px; text-align:left"><b>Apellidos</b></td>
		<td style="border: 1px solid #666; background-color:white; width:60px; text-align:left"><b>Presente</b></td>
		<td style="border: 1px solid #666; background-color:white; width:110px; text-align:left"><b>Observacion:</b></td>

		</tr>

	</table>

EOF;

$pdf->writeHTML($bloque2, false, false, false, false, '');

// ---------------------------------------------------------
foreach ($listado["detalle"] as $key => $item) {

$bloque3 = <<<EOF

	<table style="font-size:10px; padding:2px 3px;">

		<tr>
		
		<td style="border: 1px solid #666; background-color:white; width:70px; text-align:left">$item[codigo_alumno]</td>
		<td style="border: 1px solid #666; background-color:white; width:150px; text-align:left">$item[nombre_alumno]</td>
		<td style="border: 1px solid #666; background-color:white; width:150px; text-align:left">$item[apellido_alumno]</td>
		<td style="border: 1px solid #666; background-color:white; width:60px; text-align:center"></td>
		<td style="border: 1px solid #666; background-color:white; width:110px; text-align:center"></td>

		</tr>

	</table>

EOF;

$pdf->writeHTML($bloque3, false, false, false, false, '');
}
// ---------------------------------------------------------
$bloque4 = <<<EOF
	<table style="font-size:11px; padding:2px 3px;">

		<tr>
			<td style="border-bottom: 1px solid #666; background-color:white; width:540px">
				<div style="font-size:12px; text-align:left; line-height:15px;">
					<b>3. Alumno nuevos</b>
				</div>			
			</td>

		</tr>

		<tr>
		
			<td style="border: 1px solid #666; background-color:white; width:180px; text-align:left"><b>Nombres</b></td>
			<td style="border: 1px solid #666; background-color:white; width:180px; text-align:left"><b>Apellidos</b></td>
			<td style="border: 1px solid #666; background-color:white; width:180px; text-align:left"><b>Fecha Nacimiento</b></td>
		</tr>

		<tr>
		
			<td style="border: 1px solid #666; background-color:white; width:180px; text-align:left"></td>
			<td style="border: 1px solid #666; background-color:white; width:180px; text-align:left"></td>
			<td style="border: 1px solid #666; background-color:white; width:180px; text-align:left"></td>
		</tr>

		<tr>
		
			<td style="border: 1px solid #666; background-color:white; width:180px; text-align:left"></td>
			<td style="border: 1px solid #666; background-color:white; width:180px; text-align:left"></td>
			<td style="border: 1px solid #666; background-color:white; width:180px; text-align:left"></td>
		</tr>

		<tr>
		
			<td style="border: 1px solid #666; background-color:white; width:180px; text-align:left"></td>
			<td style="border: 1px solid #666; background-color:white; width:180px; text-align:left"></td>
			<td style="border: 1px solid #666; background-color:white; width:180px; text-align:left"></td>
		</tr>
	</table>
EOF;

$pdf->writeHTML($bloque4, false, false, false, false, '');
// ---------------------------------------------------------
$bloque5 = <<<EOF
	<table>
		<tr>
			<td style="width:180">
				<div style="font-size:12px; text-align:center; line-height:15px;">
					<br>
					<br>
					____________________
					<br>Pastor
					<br>RUBEN LOPEZ
				</div>
			</td>

			<td style="width:180">
				<div style="font-size:12px; text-align:center; line-height:15px;">
					<br>
					<br>
					____________________
					<br>Secretario
					<br>JONNY MARTINEZ
				</div>
			</td>

			<td style="width:180">
				<div style="font-size:12px; text-align:center; line-height:15px;">
					<br>
					<br>
					____________________
					<br>Maestro
					<br>$maestro
				</div>
			</td>
		</tr>
	</table>
EOF;
$pdf->writeHTML($bloque5, false, false, false, false, '');
// ---------------------------------------------------------

$pdf->Output('listado.pdf');

}
}

$list = new imprimirFormato();
$list -> idClase = $_GET["clase"];
$list -> anio = $_GET["anio"];
$list -> traerFormato();
?>