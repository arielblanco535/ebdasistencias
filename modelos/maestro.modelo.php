<?php

require_once "conexion.php";

class ModeloMaestro{

	/*=============================================
	MOSTRAR MAESTROS	
	=============================================*/
	static public function mdlMostrarMaestro(){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'http://reportesebd.com/maestros',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$json = json_decode($response, true);

		return $json;

	}

	static public function mdlCrearMaestro($datos){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'http://reportesebd.com/maestros',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => 'nombre='.$datos["nombre"].'&apellido='.$datos["apellido"].'&fecha_nacimiento='.$datos["fecha"].'&cedula='.$datos["cedula"].'&id_clase='.$datos["idClase"],
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/x-www-form-urlencoded'
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$json = json_decode($response, true);
		return $json;
	}

	static public function mdlActualizarMaestro($item1, $valor1, $item2, $valor2, $idClase){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'http://reportesebd.com/maestros/'.$idClase,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => 'item1='.$item1.'&valor1='.$valor1.'&item2='.$item2.'&valor2='.$valor2,
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/x-www-form-urlencoded'
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$json = json_decode($response, true);
		return $json;
	}

	/*=============================================
	BUSCAR MAESTRO UNICO PARA EDITAR
	=============================================*/
	static public function mdlShowMaestro($tablas, $item, $valor){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tablas WHERE $item = :$item");

		$stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetch();			

		$stmt -> close();

		$stmt = null;

	}

	static public function mdlEditarMaestro($datos){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'http://reportesebd.com/maestros/'.$datos["idMaestro"],
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'PUT',
		  CURLOPT_POSTFIELDS => 'nombre='.$datos["nombre"].'&apellido='.$datos["apellido"].'&fecha='.$datos["fecha"].'&cedula='.$datos["cedula"].'&idClase='.$datos["idClase"],
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/x-www-form-urlencoded'
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$json = json_decode($response, true);
		return $json;
	}

	static public function mdlBorrarMaestro($id){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'http://reportesebd.com/maestros/'.$id,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'DELETE',
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$json = json_decode($response, true);
		return $json;

	}
	
}