<?php

require_once "conexion.php";

class ModeloMatricula{

	/*=============================================
	MOSTRAR MAESTROS	
	=============================================*/
	static public function mdlMostrarMatricula(){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'http://reportesebd.com/matricula',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$json = json_decode($response, true);
		return $json;
	}

	static public function mdlCrearMatricula($datos){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'http://reportesebd.com/matricula',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => 'clase='.$datos["clase"].'&alumno='.$datos["alumno"].'&periodo='.$datos["periodo"].'&fecha='.$datos["fecha"].'&estado='.$datos["estado"],
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/x-www-form-urlencoded'
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$json = json_decode($response, true);
		return $json;
	}

	/*=============================================
	BUSCAR MATRICULA UNICA PARA EDITAR
	=============================================*/
	static public function mdlShow($tablas, $item, $valor){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tablas WHERE $item = :$item");

		$stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetch();			

		$stmt -> close();

		$stmt = null;

	}

	static public function mdlActualizarMatricula($item1, $valor1, $item2, $valor2, $idClase){
	}

	/*=============================================
	BUSCAR MAESTRO UNICO PARA EDITAR
	=============================================*/
	static public function mdlShowMaestro($tablas, $item, $valor){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tablas WHERE $item = :$item");

		$stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetch();			

		$stmt -> close();

		$stmt = null;

	}

	static public function mdlEditarMatricula($datos){
	}

	static public function mdlBorrarMatricula($id){

	}
	
}