<?php

require_once "controladores/plantilla.controlador.php";
require_once "controladores/clase.controlador.php";
require_once "controladores/periodo.controlador.php";
require_once "controladores/maestro.Controlador.php";
require_once "controladores/encuentro.controlador.php";
require_once "controladores/ofrenda.controlador.php";
require_once "controladores/usuarios.controlador.php";
require_once "controladores/alumno.controlador.php";
require_once "controladores/matricula.controlador.php";
require_once "controladores/asistencia.controlador.php";

require_once "modelos/clase.modelo.php";
require_once "modelos/periodo.modelo.php";
require_once "modelos/maestro.modelo.php";
require_once "modelos/encuentro.modelo.php";
require_once "modelos/ofrenda.modelo.php";
require_once "modelos/usuarios.modelo.php";
require_once "modelos/alumno.modelo.php";
require_once "modelos/matricula.modelo.php";
require_once "modelos/asistencia.modelo.php";

$plantilla = new ControladorPlantilla();
$plantilla -> ctrPlantilla();