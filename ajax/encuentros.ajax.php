<?php

require_once "../controladores/encuentro.controlador.php";
require_once "../modelos/encuentro.modelo.php";

class AjaxEncuentro{

	/*=============================================
	EDITAR ENCUENTROS
	=============================================*/	
	public $idEncuentro;

	public function AjaxEdidarEncuentros(){
		$item = "id_encuentro";
		$valor = $this->idEncuentro;

		$respuesta = ControladorEncuentro::ctrShowEncuentro($item, $valor);

		echo json_encode($respuesta);
	}	
}
/*=============================================
EDITAR Encuentros
=============================================*/
if(isset($_POST["idEncuentro"])){

	$editar = new AjaxEncuentro();
	$editar -> idEncuentro = $_POST["idEncuentro"];
	$editar -> AjaxEdidarEncuentros();
}
