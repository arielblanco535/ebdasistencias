<?php

require_once "../controladores/maestro.Controlador.php";
require_once "../modelos/maestro.modelo.php";

class AjaxMaestro{

	/*=============================================
	EDITAR MAESTROS
	=============================================*/	
	public $idMaestro;

	public function AjaxEdidarMaestros(){
		$item = "id_maestro";
		$valor = $this->idMaestro;

		$respuesta = ControladorMaestro::ctrShowMaestro($item, $valor);

		echo json_encode($respuesta);
	}	

	/*=============================================
	ACTIVAR MAESTRO
	=============================================*/	

	public $activarMaestro;
	public $activarId;
	public $idClase;

	public function ajaxActivarMaestro(){

		$item1 = "estado";
		$valor1 = $this->activarMaestro;

		$item2 = "id_maestro";
		$valor2 = $this->activarId;

		$valor3 = $this->idClase;

		$respuesta = ModeloMaestro::mdlActualizarMaestro($item1, $valor1, $item2, $valor2, $valor3);

		echo json_encode($respuesta);
	}

	/*=============================================
	TITULAR MAESTRO
	=============================================*/	

	public $titularMaestro;
	public $titularId;
	public $claseId;

	public function ajaxTitularMaestro(){

		$item1 = "titular";
		$valor1 = $this->titularMaestro;

		$item2 = "id_maestro";
		$valor2 = $this->titularId;

		$valor3 = $this->claseId;

		$respuesta = ModeloMaestro::mdlActualizarMaestro($item1, $valor1, $item2, $valor2, $valor3);

		echo json_encode($respuesta);
	}
}
/*=============================================
EDITAR MAESTROS
=============================================*/
if(isset($_POST["idMaestro"])){

	$editar = new AjaxMaestro();
	$editar -> idMaestro = $_POST["idMaestro"];
	$editar -> AjaxEdidarMaestros();
}

/*=============================================
ACTIVAR MAESTRO
=============================================*/	
if(isset($_POST["activarMaestro"])){

	$activar = new AjaxMaestro();
	$activar -> activarMaestro = $_POST["activarMaestro"];
	$activar -> activarId = $_POST["activarId"];
	$activar -> idClase = $_POST["idClase"];
	$activar -> ajaxActivarMaestro();
}

/*=============================================
TITULAR MAESTRO
=============================================*/	
if(isset($_POST["titularMaestro"])){

	$titular = new AjaxMaestro();
	$titular -> titularMaestro = $_POST["titularMaestro"];
	$titular -> titularId = $_POST["titularId"];
	$titular -> claseId = $_POST["idClase"];
	$titular -> ajaxTitularMaestro();
}
