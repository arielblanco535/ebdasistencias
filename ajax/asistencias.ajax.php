<?php

require_once "../controladores/asistencia.controlador.php";
require_once "../modelos/asistencia.modelo.php";

class AjaxAsistencia{

	/*=============================================
	GURDAR ASISTENCIA
	=============================================*/	

	public $idAsistencia;
	public $estado;
	public $encuentro;
	public $matricula;
	public function AjaxGuadarAsistencia(){
		$item = $this->idAsistencia;
		$valor1 = $this->estado;
		$valor2 = $this->encuentro;
		$valor3 = $this->matricula;

		if($item == 0){
			$respuesta = ControladorAsistencia::ctrCrearAsistencia($item,$valor1,$valor2, $valor3);
			echo json_encode($respuesta);
		}else{
			$respuesta = ControladorAsistencia::ctrEditarAsistencia($item,$valor1,$valor2, $valor3);
			echo json_encode($respuesta);
		}
	}	

}

/*=============================================
GUARDAR ASISTENCIA
=============================================*/
if(isset($_POST["idAsistencia"])){

	$editar = new AjaxAsistencia();
	$editar -> idAsistencia = $_POST["idAsistencia"];
	$editar -> estado = $_POST["estadoAsistencia"];
	$editar -> encuentro = $_POST["idEncuentro"];
	$editar -> matricula = $_POST["idMatricula"];
	$editar -> AjaxGuadarAsistencia();
}
