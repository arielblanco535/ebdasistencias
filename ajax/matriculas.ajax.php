<?php

require_once "../controladores/matricula.controlador.php";
require_once "../modelos/matricula.modelo.php";

class AjaxMatricula{

	/*=============================================
	EDITAR MATRICULA
	=============================================*/	
	public $idMatricula;

	public function AjaxEditarMatriculas(){
		$item = "id_matricula"
		$valor = $this->idMatricula;

		$respuesta = ControladorMatricula::ctrShow("v_listadoalumnos", $item, $valor);

		echo json_encode($respuesta);
	}	

	/*=============================================
	EDITAR MATRICULA
	=============================================*/	
	public $matriculaId;

	public function AjaxEditMatriculas(){
		$item = "id_matricula"
		$valor = $this->matriculaId;

		$respuesta = ControladorMatricula::ctrShow("matricula", "id_matricula", 292);

		echo json_encode($respuesta);
	}	
}
/*=============================================
EDITAR MATRICULA
=============================================*/
if(isset($_POST["idMatricula"])){

	$editar = new AjaxMatricula();
	$editar -> idMatricula = $_POST["idMatricula"];
	$editar -> AjaxEditarMatriculas();
}

/*=============================================
EDITAR MATRICULA
=============================================*/
if(isset($_POST["matriculaId"])){

	$editar = new AjaxMatricula();
	$editar -> matriculaId = $_POST["matriculaId"];
	$editar -> AjaxEditMatriculas();
}
