<?php

require_once "../controladores/ofrenda.controlador.php";
require_once "../modelos/ofrenda.modelo.php";

class AjaxOfrenda{

	/*=============================================
	EDITAR OFRENDAS
	=============================================*/	
	public $idOfrenda;

	public function AjaxEdidarOfrendas(){
		$item = "id_ofrenda";
		$valor = $this->idOfrenda;

		$respuesta = ControladorOfrenda::ctrShowOfrendaUnica($item, $valor);

		echo json_encode($respuesta);
	}	
}
/*=============================================
EDITAR OFRENDAS
=============================================*/
if(isset($_POST["idOfrenda"])){

	$editar = new AjaxOfrenda();
	$editar -> idOfrenda = $_POST["idOfrenda"];
	$editar -> AjaxEdidarOfrendas();
}
