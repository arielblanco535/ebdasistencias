<?php

require_once "../controladores/alumno.controlador.php";
require_once "../modelos/alumno.modelo.php";

class AjaxAlumno{

	/*=============================================
	EDITAR ALUMNOS
	=============================================*/	
	public $idAlumno;

	public function AjaxEdidarAlumnos(){
		$item = "id_alumno";
		$valor = $this->idAlumno;

		$respuesta = ControladorAlumno::ctrShowAlumno($item, $valor);

		echo json_encode($respuesta);
	}	
}
/*=============================================
EDITAR ALUMNOS
=============================================*/
if(isset($_POST["idAlumno"])){

	$editar = new AjaxAlumno();
	$editar -> idAlumno = $_POST["idAlumno"];
	$editar -> AjaxEdidarAlumnos();
}
