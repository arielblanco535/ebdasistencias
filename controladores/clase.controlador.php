<?php

class ControladorClase{

	/*=============================================
	MOSTRAR CLASES
	=============================================*/
	static public function ctrMostrarClase($valor){
		if($valor == 0){
			$respuesta = ModeloClase::mdlMostrarClase();
			return $respuesta;
		} else {
			$respuesta = ModeloClase::mdlMostrarClase();
			foreach ($respuesta["detalle"] as $key => $value) {
				if($value["id_clase"] == $valor){
					return $value["nombre"];
				}
			}
		}
	}
}