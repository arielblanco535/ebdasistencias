<?php

class ControladorOfrenda{

	/*=============================================
	MOSTRAR CLASES
	=============================================*/

	static public function ctrMostrarOfrenda($fecha, $id){

		if($fecha == ""){
			$ofrenda = ModeloOfrenda::mdlMostrarOfrenda();

			return $ofrenda;
		} else {
			$ofrenda = ModeloOfrenda::mdlMostrarOfrenda();
			$o = "";
			foreach ($ofrenda["detalle"] as $key => $value) {
				if($value["fecha_recaudado"] == $fecha && $value["id_clase"] == $id){
					return $value["monto"];
					break;
				}
			}
			
		}
	}

	static public function ctrShowOfrenda($id){
		$respuesta = ModeloOfrenda::mdlShowOfrenda($id);
		return $respuesta;
	}

	static public function ctrShowOfrendaUnica($item, $valor){
		$tablas = "ofrenda";
		$respuesta = ModeloOfrenda::mdlShowOfrendaUnica($tablas, $item, $valor);
		return $respuesta;
	}

	static public function ctrCrearOfrenda(){
		if(isset($_POST["nuevaFechaOfrenda"])){

			if(date("m", strtotime($_POST["nuevaFechaOfrenda"])) < date("m")){
				echo '<script>

						swal({

							type: "warning",
							title: "¡La fecha no puede ser de un mes anterior!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"

						}).then(function(result){

							if(result.value){

								window.location = "encuentroofrenda";

							}

						});
					

						</script>';
				
			} else {
				$respuesta = ModeloOfrenda::mdlCrearOfrenda($_POST["nuevoMonto"], $_POST["nuevaFechaOfrenda"], $_POST["nuevaClase"]);

				if($respuesta["status"] == 300){
					echo '<script>

							swal({

								type: "error",
								title: "¡La ofrenda ya esta registrada para esta clase!",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"

							}).then(function(result){

								if(result.value){
								
									window.location = "encuentroofrenda";

								}

							});
						

							</script>';
				} else if($respuesta["status"] == 200){
					echo '<script>

							swal({

								type: "success",
								title: "¡La ofrenda se ha registrado correctamente!",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"

							}).then(function(result){

								if(result.value){
								
									window.location = "encuentroofrenda";

								}

							});
						

							</script>';

				} else {
					echo '<script>

							swal({

								type: "warning",
								title: "¡Este encuentro no esta registrado¡",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"

							}).then(function(result){

								if(result.value){
								
									window.location = "encuentroofrenda";

								}

							});
						

							</script>';

				}
			}
		}
	}

	static public function ctrEditarOfrenda(){
		if(isset($_POST["editarFechaOfrenda"])){
			$respuesta = ModeloOfrenda::mdlEditarOfrenda($_POST["editarIdOfrenda"], $_POST["editarMonto"], $_POST["editarFechaOfrenda"], $_POST["editarClase"]);

			if($respuesta["status"] == 200){
				echo '<script>

						swal({

							type: "success",
							title: "¡La ofrenda se ha modificado!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"

						}).then(function(result){

							if(result.value){
							
								window.location = "encuentroofrenda";

							}

						});
					

						</script>';
			} else {
				echo '<script>

						swal({

							type: "Error",
							title: "La ofrenda no se ha modificado!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"

						}).then(function(result){

							if(result.value){
							
								window.location = "encuentroofrenda";

							}

						});
					

						</script>';

			}
		}
	}

	/*=============================================
	BORRAR OFRENDA
	=============================================*/
	static public function ctrBorrarOfrenda(){

		if(isset($_GET["idOfrenda"])){

			$datos = $_GET["idOfrenda"];

			$respuesta = ModeloOfrenda::mdlBorrarOfrenda($datos);


			if($respuesta["status"] == 300){

				echo'<script>

				swal({
					  type: "warning",
					  title: "Ha ocurrido un problema al borrar, no se puede eliminar",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "encuentroofrenda";

								}
							})

				</script>';

			} else {
				echo'<script>

				swal({
					  type: "success",
					  title: "La ofrenda ha sido borrado con exito",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "encuentroofrenda";

								}
							})

				</script>';

			}		

		}

	}

}