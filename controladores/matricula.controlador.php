<?php

class ControladorMatricula{

	/*=============================================
	MOSTRAR MATRICULAS
	=============================================*/

	static public function ctrMostrarMatricula(){

		$respuesta = ModeloMatricula::mdlMostrarMatricula();
		return $respuesta;
	}

	/*=============================================
	MOSTRAR UNA SOLA MATRICULA
	=============================================*/

	static public function ctrShow($table, $item, $valor){
		$tabla = $table;
		$respuesta = ModeloMatricula::mdlShow($tabla, $item, $valor);
		return $respuesta;
	}

	/*=============================================
	CREAR MATRICULA
	=============================================*/
	static public function ctrCrearMatricula(){
		if(isset($_POST["matriculaCodigoAlumno"])){
			$datos = array("clase"=>$_POST["matriculaClaseAlumno"],
							"alumno"=>strtoupper($_POST["matriculaIdAlumno"]),
							"periodo"=>strtoupper($_POST["matriculaPeriodoAlumno"]),
							"fecha"=>$_POST["matriculaFechaRegistroAlumno"],
							"estado"=>$_POST["matriculaEstadoAlumno"]);

			$alumno = ModeloMatricula::mdlCrearMatricula($datos);

			if($alumno["status"] == 200){
				echo '<script>

					swal({

						type: "success",
						title: "¡El alumno se ha matriculado correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "matricula";

						}

					});
					

				</script>';

			} else if($alumno["status"] == 404){
				echo '<script>

					swal({

						type: "warning",
						title: "¡El alumno ya esta matriculado!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "matricula";

						}

					});
					

				</script>';

			} else {
				echo '<script>

					swal({

						type: "warning",
						title: "¡El año no puede ser uno anterior al actual!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "matricula";

						}

					});
					

				</script>';

			}
		}
	}

}