<?php

class ControladorAlumno{

	/*=============================================
	MOSTRAR LISTADO DE ALUMNOS
	=============================================*/
	static public function ctrMostrarAlumnos(){
		$alumno = ModeloAlumno::mdlMostrarAlumnos();
		return $alumno;
	}

	/*=============================================
	CREAR ALUMNO
	=============================================*/
	static public function ctrCrearAlumno(){
		/*=============================================
		VALIDAR SOLO LETRAS EN EL NOMBRE
		=============================================*/
		if(isset($_POST["nuevoNombreAlumno"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $_POST["nuevoNombreAlumno"])||
			isset($_POST["nuevoApellidoAlumno"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $_POST["nuevoApellidoAlumno"])){
			echo '<script>

				swal({

					type: "warning",
					title: "¡Advertencia en el campo nombre y apellido, solo se permiten letras!",
					showConfirmButton: true,
					confirmButtonText: "Cerrar"

				}).then(function(result){

					if(result.value){

						window.location = "alumno";

					}

				});
				

			</script>';
			return;
		}

		if(isset($_POST["nuevoNombreAlumno"])){
			$codigo = date("d",strtotime($_POST["nuevaFechaNac"]))."".date("m",strtotime($_POST["nuevaFechaNac"]))."".date("y",strtotime($_POST["nuevaFechaNac"]))."-".rand(10,99);
			$datos = array("codigo"=>$codigo,
							"nombre"=>strtoupper($_POST["nuevoNombreAlumno"]),
							"apellido"=>strtoupper($_POST["nuevoApellidoAlumno"]),
							"fechaNac"=>$_POST["nuevaFechaNac"],
							"fechaIng"=>$_POST["nuevaFechaIng"]);

			$alumno = ModeloAlumno::mdlCrearAlumno($datos);

			if($alumno["status"] == 200){
				echo '<script>

					swal({

						type: "success",
						title: "¡El alumno se ha registrado correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "alumno";

						}

					});
					

				</script>';

			} else {
				echo '<script>

					swal({

						type: "warning",
						title: "¡El alumno ya esta registrado!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "alumno";

						}

					});
					

				</script>';

			}
		}
	}

	/*=============================================
	BUSCAR ALUMNO UNICO PARA EDITAR
	=============================================*/
	static public function ctrShowAlumno($item, $valor){
		$tablas = "alumno";
		$respuesta = ModeloAlumno::mdlShowAlumno($tablas, $item, $valor);
		return $respuesta;
	}

	/*=============================================
	EDITAR ALUMNO
	=============================================*/
	static public function ctrEditarAlumno(){
		/*=============================================
		VALIDAR SOLO LETRAS EN EL NOMBRE
		=============================================*/
		if(isset($_POST["editarNombreAlumno"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $_POST["editarNombreAlumno"])||
			isset($_POST["editarApellidoAlumno"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $_POST["editarApellidoAlumno"])){
			echo '<script>

				swal({

					type: "warning",
					title: "¡Advertencia en el campo nombre y apellido, solo se permiten letras!",
					showConfirmButton: true,
					confirmButtonText: "Cerrar"

				}).then(function(result){

					if(result.value){

						window.location = "alumno";

					}

				});
				

			</script>';
			return;
		}

		if(isset($_POST["editarNombreAlumno"])){
			$datos = array("idAlumno"=>$_POST["editarIdAlumno"],
							"nombre"=>strtoupper($_POST["editarNombreAlumno"]),
							"apellido"=>strtoupper($_POST["editarApellidoAlumno"]),
							"fechaNac"=>$_POST["editarFechaNac"],
							"fechaIng"=>$_POST["editarFechaIng"]);

			$alumno = ModeloAlumno::mdlEditarAlumno($datos);

			if($alumno["status"] == 200){
				echo '<script>

					swal({

						type: "success",
						title: "¡El alumno se ha modificado correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "alumno";

						}

					});
					

				</script>';

			} else {
				echo '<script>

					swal({

						type: "error",
						title: "¡Error al modifcar el alumno¡",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "alumno";

						}

					});
					

				</script>';

			}
		}
	}

	/*=============================================
	MOSTRAR LISTA FORMATO
	=============================================*/
	static public function ctrMostrarFormato($idClase, $anio){

		$Alumno = ModeloAlumno::mdlMostrarFormato($idClase, $anio);
		return $Alumno;
	}

	/*=============================================
	BORRAR ALUMNOS
	=============================================*/
	static public function ctrBorrarAlumno(){

		if(isset($_GET["idAlumno"])){

			$datos = $_GET["idAlumno"];

			$respuesta = ModeloAlumno::mdlBorrarAlumno($datos);


			if($respuesta["total_registros"] > 0){

				echo'<script>

				swal({
					  type: "warning",
					  title: "¡El alumno esta matriculado, no se puede eliminar!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "alumno";

								}
							})

				</script>';

			} else {
				echo'<script>

				swal({
					  type: "success",
					  title: "El alumno ha sido borrado con exito",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "alumno";

								}
							})

				</script>';

			}		

		}

	}
}