<?php

class ControladorMaestro{

	/*=============================================
	MOSTRAR MAESTROS
	=============================================*/

	static public function ctrMostrarMaestro($valor){

		$nombre = "";
		if($valor == 0){
			$respuesta = ModeloMaestro::mdlMostrarMaestro();
			return $respuesta;
		} else {
			$respuesta = ModeloMaestro::mdlMostrarMaestro();
			foreach ($respuesta["detalle"] as $key => $value) {
				if($value["id_clase"] == $valor && $value["titular"] == 1){
					$nom1 = explode(" ", $value["nombre_maestro"]);
					$ape1 = explode(" ", $value["apellido_maestro"]);
					$nombre = $nom1[0]." ".$ape1[0];
				}
			}
			if($nombre != "")
				return $nombre;
			else{
				echo '<script>

					swal({

						type: "error",
						title: "¡No hay maestro titular en esta clase!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){
						
							window.location = "listaClase";

						}

					});
				

					</script>';
			}
		}
	}

	/*=============================================
	CREAR MAESTRO
	=============================================*/
	static public function ctrCrearMaestro(){
		/*=============================================
		VALIDAR SOLO LETRAS EN EL NOMBRE
		=============================================*/
		if(isset($_POST["nuevoNombreMaestro"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $_POST["nuevoNombreMaestro"])||
			isset($_POST["nuevoApellidoMaestro"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $_POST["nuevoApellidoMaestro"])){
			echo '<script>

				swal({

					type: "warning",
					title: "¡Advertencia en el campo nombre y apellido, solo se permiten letras!",
					showConfirmButton: true,
					confirmButtonText: "Cerrar"

				}).then(function(result){

					if(result.value){

						window.location = "alumno";

					}

				});
				

			</script>';
			return;
		}

		if(isset($_POST["nuevoNombreMaestro"])){
			$datos = array("nombre"=>strtoupper($_POST["nuevoNombreMaestro"]),
							"apellido"=>strtoupper($_POST["nuevoApellidoMaestro"]),
							"fecha"=>$_POST["nuevaFechaNacimiento"],
							"cedula"=>$_POST["nuevaCedula"],
							"idClase"=>$_POST["nuevaClase"]);

			$alumno = ModeloMaestro::mdlCrearMaestro($datos);

			if($alumno["status"] == 200){
				echo '<script>

					swal({

						type: "success",
						title: "¡El maestro se ha registrado correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "maestro";

						}

					});
					

				</script>';

			} else {
				echo '<script>

					swal({

						type: "warning",
						title: "¡El maestro ya esta registrado!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "maestro";

						}

					});
					

				</script>';

			}
		}
	}

	/*=============================================
	BUSCAR MAESTRO UNICO PARA EDITAR
	=============================================*/
	static public function ctrShowMaestro($item, $valor){
		$tablas = "maestro";
		$respuesta = ModeloMaestro::mdlShowMaestro($tablas, $item, $valor);
		return $respuesta;
	}	

	/*=============================================
	EDITAR MAESTRO
	=============================================*/
	static public function ctrEditarMaestro(){
		/*=============================================
		VALIDAR SOLO LETRAS EN EL NOMBRE
		=============================================*/
		if(isset($_POST["editarNombreMaestro"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $_POST["editarNombreMaestro"])||
			isset($_POST["editarApellidoMaestro"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $_POST["editarApellidoMaestro"])){
			echo '<script>

				swal({

					type: "warning",
					title: "¡Advertencia en el campo nombre y apellido, solo se permiten letras!",
					showConfirmButton: true,
					confirmButtonText: "Cerrar"

				}).then(function(result){

					if(result.value){

						window.location = "maestro";

					}

				});
				

			</script>';
			return;
		}

		if(isset($_POST["editarIdMaestro"])){
			$respuesta = ModeloMaestro::mdlShowMaestro("maestro", "id_maestro", $_POST["editarIdMaestro"]);

			foreach ($respuesta as $key => $value) {
				if($value["id_clase"] = $_POST["editarClase"] && $value["titular"] == 1){
					echo '<script>

						swal({

							type: "warning",
							title: "¡Advertencia la clase no se puede cambiar si es titular!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"

						}).then(function(result){

							if(result.value){

								window.location = "maestro";

							}

						});
						

					</script>';	return;
				}
			}
		}

		if(isset($_POST["editarNombreMaestro"])){
			$datos = array("idMaestro"=>$_POST["editarIdMaestro"],
							"nombre"=>strtoupper($_POST["editarNombreMaestro"]),
							"apellido"=>strtoupper($_POST["editarApellidoMaestro"]),
							"fecha"=>$_POST["editarFechaNacimiento"],
							"cedula"=>$_POST["editarCedula"],
							"idClase"=>$_POST["editarClase"]);

			$maestro = ModeloMaestro::mdlEditarMaestro($datos);

			if($maestro["status"] == 200){
				echo '<script>

					swal({

						type: "success",
						title: "¡El maestro se ha modificado correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "maestro";

						}

					});
					

				</script>';

			} else {
				echo '<script>

					swal({

						type: "error",
						title: "¡Error al modifcar el maestro¡",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "maestro";

						}

					});
					

				</script>';

			}
		}
	}

	/*=============================================
	BORRAR MAESTRO
	=============================================*/
	static public function ctrBorrarMaestro(){

		if(isset($_GET["idMaestro"])){

			$datos = $_GET["idMaestro"];

			$respuesta = ModeloMaestro::mdlBorrarMaestro($datos);


			if($respuesta["status"] == 300){

				echo'<script>

				swal({
					  type: "warning",
					  title: "¡El maestro es titular, no se puede eliminar!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "maestro";

								}
							})

				</script>';

			} else {
				echo'<script>

				swal({
					  type: "success",
					  title: "El maestro ha sido borrado con exito",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "maestro";

								}
							})

				</script>';

			}		

		}

	}
}