<?php

class ControladorEncuentro{

	/*=============================================
	MOSTRAR CLASES
	=============================================*/

	static public function ctrMostrarEncuentro(){

		$respuesta = ModeloEncuentro::mdlMostrarEncuentro();

		return $respuesta;

	}

	static public function ctrShowEncuentro($item, $valor){
		$tablas = "encuentro";
		$respuesta = ModeloEncuentro::mdlShowEncuentro($tablas, $item, $valor);
		return $respuesta;
	}

	static public function ctrCrearEncuentro(){
		if(isset($_POST["nuevaFecha"])){

			if(date("m", strtotime($_POST["nuevaFecha"])) < date("m") || date("m", strtotime($_POST["nuevaFecha"])) > date("m")){
				echo '<script>

						swal({

							type: "warning",
							title: "¡El el mes del encuentro no puede ser menor o mayor al mes actual!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"

						}).then(function(result){

							if(result.value){

								window.location = "encuentroofrenda";

							}

						});
					

						</script>';

			} else {
				$respuesta = ModeloEncuentro::mdlCrearEncuentro($_POST["nuevaFecha"], $_POST["observacion"]);

				if($respuesta["status"] == 300){
					echo '<script>

							swal({

								type: "error",
								title: "¡El Encuentro ya esta registrado!",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"

							}).then(function(result){

								if(result.value){
								
									window.location = "encuentroofrenda";

								}

							});
						

							</script>';
				} else if($respuesta["status"] == 400){
					echo '<script>

							swal({

								type: "success",
								title: "¡El Encuentro se ha registrado correctamente!",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"

							}).then(function(result){

								if(result.value){
								
									window.location = "encuentroofrenda";

								}

							});
						

							</script>';

				}
			}
		}
	}

	static public function ctrEditarEncuentro(){
		if(isset($_POST["editarFechaEncuentro"])){
			$respuesta = ModeloEncuentro::mdlEditarEncuentro($_POST["editarId"], $_POST["editarFechaEncuentro"], $_POST["editarObservacion"]);

			if($respuesta["status"] == 200){
				echo '<script>

						swal({

							type: "success",
							title: "¡El Encuentro se ha modificado!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"

						}).then(function(result){

							if(result.value){
							
								window.location = "encuentroofrenda";

							}

						});
					

						</script>';
			} else {
				echo '<script>

						swal({

							type: "Error",
							title: "¡El Encuentro no se ha modificado!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"

						}).then(function(result){

							if(result.value){
							
								window.location = "encuentroofrenda";

							}

						});
					

						</script>';

			}
		}
	}

	/*=============================================
	BORRAR ENCUENTROSS
	=============================================*/
	static public function ctrBorrarEncuentro(){

		if(isset($_GET["idEncuentro"])){

			$datos = $_GET["idEncuentro"];

			$respuesta = ModeloEncuentro::mdlBorrarEncuentro($datos);


			if($respuesta["total_registros"] > 0){

				echo'<script>

				swal({
					  type: "warning",
					  title: "Hay ofrendas registradas para este encuentro, no se puede eliminar",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "encuentroofrenda";

								}
							})

				</script>';

			} else {
				echo'<script>

				swal({
					  type: "success",
					  title: "El encuentro ha sido borrado con exito",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "encuentroofrenda";

								}
							})

				</script>';

			}		

		}

	}

}