<?php

class ControladorAsistencia{

	static public function ctrMostrarAsistencia(){
		$asis = ModeloAsistencia::mdlMostrarAsistencia();
		return $asis;
	}

	/*=============================================
	CREAR ASISTENCIA
	=============================================*/
	static public function ctrCrearAsistencia($id, $estado, $idE, $idM){

		$datos = array("idAsistencia"=>$id,
							"idEncuentro"=>$idE,
							"idMatricula"=>$idM,
							"estado"=>$estado);
		$asis = ModeloAsistencia::mdlCrearAsistencia($datos);
	}

	/*=============================================
	EDITAR ASISTENCIA
	=============================================*/
	static public function ctrEditarAsistencia($id, $estado, $idE, $idM){

		$datos = array("idAsistencia"=>$id,
						"idEncuentro"=>$idE,
						"idMatricula"=>$idM,
						"estado"=>$estado);
		$asis = ModeloAsistencia::mdlEditarAsistencia($datos);
		return $asis;
	}

	static public function ctrGuardar($fecha){
		$listac = ModeloMatricula::mdlMostrarMatricula();

		$encu = ModeloEncuentro::mdlMostrarEncuentro();
	      $enct = "";
	      foreach ($encu["detalle"] as $key => $valueE) {
	        if($valueE["fecha"] == $fecha)
	          $enct = $valueE["id_encuentro"];
	      }

		foreach ($listac["detalle"] as $key => $value) {
            if($value["estado"] == "1" && $_POST["asistenciaPeriodo"] == $value["id_periodo"] && $_POST["asistenciaClase"] == $value["id_clase"]){
					$datos = array("idAsistencia"=>0,
							"idEncuentro"=>$enct,
							"idMatricula"=>$value["id_matricula"],
							"estado"=>0);
            	$s = ModeloAsistencia::mdlCrearAsistencia($datos);
            }
        }
        return;
	}

}