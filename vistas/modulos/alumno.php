<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Alumnos
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Alumnos</li>

    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
 
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalRegistrarAlumno">
          
          Registrar Alumno

        </button>
        
      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Codigo</th>
           <th>Nombre</th>
           <th>Apellido</th>
           <th>Fecha Nacimiento</th>
           <th>Acciones</th>

         </tr> 

        </thead>

        <tbody>

         <?php

            $listc = ControladorAlumno::ctrMostrarAlumnos();
            foreach ($listc["detalle"] as $key => $value) {
           
              echo ' <tr>

                    <td>'.($key+1).'</td>

                    <td>'.$value["codigo_alumno"].'</td>

                    <td>'.$value["nombre_alumno"].'</td>

                    <td>'.$value["apellido_alumno"].'</td>';
                    $date = date_create($value["fecha_nacimiento"]);
                    echo '<td>'.date_format($date, 'd-m-Y').'</td>
                    <td>

                      <div class="btn-group">
                          
                        <button class="btn btn-warning btnEditarAlumno" idAlumno="'.$value["id_alumno"].'" data-toggle="modal" data-target="#modalModificarAlumno"><i class="fa fa-pencil"></i></button>

                        <button class="btn btn-danger btnEliminarAlumno" idAlumno="'.$value["id_alumno"].'"><i class="fa fa-times"></i></button>

                      </div> 

                    </td>
                    
                  </tr>';
            }
          ?>
        </tbody>

       </table>

      <div>
        
      </div>

      <div>
      </div>

      </div>

    </div>

  </section>

</div>

<!--=====================================
MODAL REGISTRAR ALUMNO
======================================-->

<div id="modalRegistrarAlumno" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Registrar Alumno</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA EL CODIGO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-code"></i></span> 

                <input type="text" class="form-control input-lg" name="nuevoCodigo" placeholder="Codigo" style="display: none;" readonly required>

              </div>

            </div>

            <!-- ENTRADA PARA EL NOMBRE -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="nuevoNombreAlumno" name="nuevoNombreAlumno" placeholder="Ingresar nombre" required>

              </div>

            </div>

            <!-- ENTRADA PARA EL APELLIDO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="nuevoApellidoAlumno" name="nuevoApellidoAlumno" placeholder="Ingresar apellido" required>

              </div>

            </div>

            <!-- ENTRADA PARA LA FECHA NACIMIENTO-->
            
            <div class="form-group"><b>Fecha de Nacimiento</b>
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="nuevaFechaNac" name="nuevaFechaNac" placeholder="" required>

              </div>

            </div>

            <!-- ENTRADA PARA LA FECHA INGRESO-->
            
            <div class="form-group"><b>Fecha de Ingreso</b>
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="nuevaFechaIng" name="nuevaFechaIng" placeholder="" required>

              </div>

            </div>

          </div>
        </div>
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Registrar Alumno</button>

        </div>

        <?php
          $crearAlumno = new ControladorAlumno();
          $crearAlumno -> ctrCrearAlumno();
        ?>

      </form>

    </div>

  </div>

</div>

<!--=====================================
MODAL MODIFICAR ALUMNO
======================================-->

<div id="modalModificarAlumno" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Modificar Alumno</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA EL ID -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <input type="text" class="form-control input-lg" name="editarIdAlumno" id="editarIdAlumno" style="display: none;" readonly required>

              </div>
            <!-- ENTRADA PARA EL CODIGO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-code"></i></span> 

                <input type="text" class="form-control input-lg" name="editarCodigo" id="editarCodigo" readonly required>

              </div>

            </div>

            <!-- ENTRADA PARA EL NOMBRE -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="editarNombreAlumno" name="editarNombreAlumno" placeholder="Ingresar nombre" required>

              </div>

            </div>

            <!-- ENTRADA PARA EL APELLIDO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="editarApellidoAlumno" name="editarApellidoAlumno" placeholder="Ingresar apellido" required>

              </div>

            </div>

            <!-- ENTRADA PARA LA FECHA NACIMIENTO-->
            
            <div class="form-group"><b>Fecha de Nacimiento</b>
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="editarFechaNac" name="editarFechaNac" placeholder="" required>

              </div>

            </div>

            <!-- ENTRADA PARA LA FECHA INGRESO-->
            
            <div class="form-group"><b>Fecha de Ingreso</b>
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="editarFechaIng" name="editarFechaIng" placeholder="" required>

              </div>

            </div>

          </div>
        </div>
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Modificar Alumno</button>

        </div>

        <?php
          $editarAlumno = new ControladorAlumno();
          $editarAlumno -> ctrEditarAlumno();
        ?>

      </form>

    </div>

  </div>

</div>

<?php
  $borrarAlumno = new ControladorAlumno();
  $borrarAlumno -> ctrBorrarAlumno();
?>
