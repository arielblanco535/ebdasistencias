<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Matricula
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Matricula</li>

    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
 
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalFiltrarLista">
          
          Filtrar Lista

        </button>
        
      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Codigo</th>
           <th>Nombres</th>
           <th>Apellidos</th>
           <th>Fecha Nacimiento</th>
           <th>Acciones</th>

         </tr> 

        </thead>

        <tbody>

         <?php

          if(!isset($_POST["filtrarClaseAlumno"]) || $_POST["filtrarClaseAlumno"] == 0){
            $list = ControladorAlumno::ctrMostrarAlumnos();
            foreach ($list["detalle"] as $key => $value) {
           
              echo ' <tr>

                    <td>'.($key+1).'</td>

                    <td>'.$value["codigo_alumno"].'</td>

                    <td>'.$value["nombre_alumno"].'</td>

                    <td>'.$value["apellido_alumno"].'</td>';

                    $date = date_create($value["fecha_nacimiento"]);
                    echo '<td>'.date_format($date, 'd-m-Y').'</td>';
 
                  echo '<td>

                      <div class="btn-group">
                          
                        <button class="btn btn-success btnMatriculaAlumno" idAlumno="'.$value["id_alumno"].'" data-toggle="modal" data-target="#modalMatricular"><i class="glyphicon glyphicon-list-alt"></i></button>

                      </div> 

                    </td>

                      
                  </tr>';
            }
          } else {
            $listM = ControladorMatricula::ctrMostrarMatricula();
            foreach ($listM["detalle"] as $key => $value) {
           
              if($_POST["filtrarEstadoAlumno"] == $value["estado"] && $_POST["filtrarPeriodoAlumno"] == $value["id_periodo"] && $_POST["filtrarClaseAlumno"] == $value["id_clase"]){
                echo ' <tr>

                      <td>'.($key+1).'</td>

                      <td>'.$value["codigo_alumno"].'</td>

                      <td>'.$value["nombre_alumno"].'</td>

                      <td>'.$value["apellido_alumno"].'</td>';

                      $date = date_create($value["fecha_nacimiento"]);
                      echo '<td>'.date_format($date, 'd-m-Y').'</td>';
   
                    echo '<td>

                        <div class="btn-group">
                            
                          <button class="btn btn-warning btnEditarMatricula" idMatricula="'.$value["id_matricula"].'" data-toggle="modal" data-target="#modalEditMatri"><i class="fa fa-pencil"></i></button>

                        </div> 

                      </td>

                        
                    </tr>';
              }
            }
          }
        ?>
        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>

<!--=====================================
MODAL FILTRAR LISTA
======================================-->
<div id="modalFiltrarLista" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Filtrar lista</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

           <!-- ENTRADA PARA SELECCIONAR CLASE -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="filtrarClaseAlumno" name="filtrarClaseAlumno" required>
                  
                  <option value="">SELECCIONAR CLASE</option>
                  <option value"0">TODOS</option>

                  <?php

                  $clase = ControladorClase::ctrMostrarClase(0);

                  foreach ($clase["detalle"] as $key => $value) {
                    
                    echo '<option value="'.$value["id_clase"].'">'.$value["nombre"].'</option>';
                  }

                  ?>
  
                </select>
              </div>
            </div>

            <!-- ENTRADA PARA SELECCIONAR ESTADO -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="filtrarEstadoAlumno" name="filtrarEstadoAlumno" required>
                  
                  <option value="">SELECCIONAR ESTADO</option>

                  <option value="100">TODOS</option>

                  <option value="1">ACTIVO</option>

                  <option value="0">INACTIVO</option>

  
                </select>
              </div>
            </div>

           <!-- ENTRADA PARA SELECCIONAR PERIODO -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="filtrarPeriodoAlumno" name="filtrarPeriodoAlumno" required>
                  
                  <?php

                  $orden = "nombre";

                  $periodo = ControladorPeriodo::ctrMostrarPeriodo();

                  foreach ($periodo["detalle"] as $key => $value) {
                    
                    echo '<option value="'.$value["id_periodo"].'">'.$value["anio"].'</option>';
                  }

                  ?>
  
                </select>
              </div>
            </div>

          </div>
        </div>
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Buscar</button>

        </div>

      </form>

    </div>

  </div>

</div>

<!--=====================================
MODAL MATRICULAR
======================================-->
<div id="modalMatricular" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Crear Matricula</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

           <!-- ENTRADA PARA EL ID -->
           
            <div class="form-group">
              
              <div class="input-group">
              
                <input type="text" class="form-control input-lg" name="matriculaIdAlumno" id="matriculaIdAlumno" style="display: none;" readonly required>

              </div>

            </div>

            <!-- ENTRADA PARA EL CODIGO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-code"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="matriculaCodigoAlumno" name="matriculaCodigoAlumno" placeholder="Codigo" readonly required>

              </div>

            </div>

            <!-- ENTRADA PARA EL NOMBRE -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="matriculaNombreAlumno" name="matriculaNombreAlumno" placeholder="Nombre" readonly required>

              </div>

            </div>

            <!-- ENTRADA PARA EL APELLIDO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="matriculaApellidoAlumno" name="matriculaApellidoAlumno" placeholder="Apellido" readonly required>

              </div>

            </div>

            <!-- ENTRADA PARA LA FECHA NACIMIENTO-->
            
            <div class="form-group"><b>Fecha de Nacimiento</b>
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="matriculaFechaNacimientoAlumno" name="matriculaFechaNacimientoAlumno" placeholder="" readonly required>

              </div>

            </div>

            <!-- ENTRADA PARA LA FECHA DE MATRICULA-->
            
            <div class="form-group"><b>Fecha de Matricula</b>
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="matriculaFechaRegistroAlumno" name="matriculaFechaRegistroAlumno" placeholder="" required>

              </div>

            </div>

           <!-- ENTRADA PARA SELECCIONAR CLASE -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="matriculaClaseAlumno" name="matriculaClaseAlumno" required>
                  
                  <option value="">SELECCIONAR CLASE</option>

                  <?php

                  $clase = ControladorClase::ctrMostrarClase(0);

                  foreach ($clase["detalle"] as $key => $value) {
                    
                    echo '<option value="'.$value["id_clase"].'">'.$value["nombre"].'</option>';
                  }

                  ?>
  
                </select>
              </div>
            </div>

            <!-- ENTRADA PARA SELECCIONAR ESTADO -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="matriculaEstadoAlumno" name="matriculaEstadoAlumno" required>
                  
                  <option value="">SELECCIONAR ESTADO</option>

                  <option value="1">ACTIVO</option>

                  <option value="0">INACTIVO</option>

  
                </select>
              </div>
            </div>

           <!-- ENTRADA PARA SELECCIONAR PERIODO -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="matriculaPeriodoAlumno" name="matriculaPeriodoAlumno" required>
                  
                  <?php

                  $orden = "nombre";

                  $periodo = ControladorPeriodo::ctrMostrarPeriodo();

                  foreach ($periodo["detalle"] as $key => $value) {
                    
                    echo '<option value="'.$value["id_periodo"].'">'.$value["anio"].'</option>';
                  }

                  ?>
  
                </select>
              </div>
            </div>


          </div>
        </div>
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Matricular</button>

        </div>

        <?php
          $crearMatricula = new ControladorMatricula();
          $crearMatricula -> ctrCrearMatricula();
        ?>

      </form>

    </div>

  </div>

</div>

<!--=====================================
MODAL EDITAR MATRICULAR
======================================-->
<div id="modalEditMatri" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Editar Matricula</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

           <!-- ENTRADA PARA EL ID -->
           
            <div class="form-group">
              
              <div class="input-group">
              
                <input type="text" class="form-control input-lg" name="editarMatriculaId" id="editarMatriculaId" style="display: none;" readonly required>

              </div>

            </div>

            <!-- ENTRADA PARA EL CODIGO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-code"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="editarMatriculaCodigo" name="editarMatriculaCodigo" placeholder="Codigo" readonly required>

              </div>

            </div>

            <!-- ENTRADA PARA EL NOMBRE -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="editarMatriculaNombre" name="EditarMatriculaNombre" placeholder="Nombre" readonly required>

              </div>

            </div>

            <!-- ENTRADA PARA EL APELLIDO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="editarMatriculaApellido" name="editarMatriculaApellido" placeholder="Apellido" readonly required>

              </div>

            </div>

            <!-- ENTRADA PARA LA FECHA NACIMIENTO-->
            
            <div class="form-group"><b>Fecha de Nacimiento</b>
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="editarMatriculaFechaNacimiento" name="editarMatriculaFechaNacimiento" placeholder="" readonly required>

              </div>

            </div>

            <!-- ENTRADA PARA LA FECHA DE MATRICULA-->
            
            <div class="form-group"><b>Fecha de Matricula</b>
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="editarMatriculaFechaRegistro" name="editarMatriculaFechaRegistro" placeholder="" required>

              </div>

            </div>

           <!-- ENTRADA PARA SELECCIONAR CLASE -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="editarMatriculaClase" name="editarMatriculaClase" required>
                  
                  <option value="">SELECCIONAR CLASE</option>

                  <?php

                  $clase = ControladorClase::ctrMostrarClase(0);

                  foreach ($clase["detalle"] as $key => $value) {
                    
                    echo '<option value="'.$value["id_clase"].'">'.$value["nombre"].'</option>';
                  }

                  ?>
  
                </select>
              </div>
            </div>

            <!-- ENTRADA PARA SELECCIONAR ESTADO -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="editarMatriculaEstado" name="editarMatriculaEstado" required>
                  
                  <option value="">SELECCIONAR ESTADO</option>

                  <option value="1">ACTIVO</option>

                  <option value="0">INACTIVO</option>

  
                </select>
              </div>
            </div>

           <!-- ENTRADA PARA SELECCIONAR PERIODO -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="editarMatriculaPeriodo" name="editarMatriculaPeriodo" required>
                  
                  <?php

                  $orden = "nombre";

                  $periodo = ControladorPeriodo::ctrMostrarPeriodo();

                  foreach ($periodo["detalle"] as $key => $value) {
                    
                    echo '<option value="'.$value["id_periodo"].'">'.$value["anio"].'</option>';
                  }

                  ?>
  
                </select>
              </div>
            </div>


          </div>
        </div>
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Matricular</button>

        </div>

      </form>

    </div>

  </div>

</div>


<?php
  $borrarMaestro = new ControladorMaestro();
  $borrarMaestro -> ctrBorrarMaestro();
?>