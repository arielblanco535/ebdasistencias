<aside class="main-sidebar">

	 <section class="sidebar">

		<ul class="sidebar-menu">

			<li class="active">

				<a href="inicio">

					<i class="fa fa-home"></i>
					<span>Inicio</span>

				</a>

			</li>

			<li>

				<a href="matricula">

					<i class="fa fa-id-card"></i>
					<span>Matricula</span>

				</a>

			</li>

			<li>

				<a href="alumno">

					<i class="fa fa-child"></i>
					<span>Alumno</span>

				</a>

			</li>

			<li>

				<a href="encuentroofrenda">

					<i class="glyphicon glyphicon-piggy-bank"></i>
					<span>Encuentro/Ofrenda</span>

				</a>

			</li>

			<li>

				<a href="asistencia">

					<i class="glyphicon glyphicon-education"></i>
					<span>Asistencia</span>

				</a>

			</li>

			<li>

				<a href="maestro">

					<i class="glyphicon glyphicon-education"></i>
					<span>Maestro</span>

				</a>

			</li>

			<li>

				<a href="claseperiodo">

					<i class="glyphicon glyphicon-blackboard"></i>
					<span>Clase/Periodo</span>

				</a>

			</li>

			<?php
				if($_SESSION["perfil"] == "Administrador"){
					echo '<li>

						<a href="usuarios">

							<i class="fa fa-user"></i>
							<span>Usuarios</span>

						</a>

					</li>';
				}
			?>
		</ul>

	 </section>

</aside>