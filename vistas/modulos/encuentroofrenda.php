<?php
?>
<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Encuentros Y Ofrendas
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Encuentros y Ofrendas</li>
    
    </ol>

  </section>

  <section class="content">
    <div class="row">
        <div class="col-md-6 col-xs-12">
          <div class="box box-success">
            
            <div class="box-header with-border"></div>
              <div class="box">
                <div class="box-header with-border">

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalCargarEncuentro">
                      Registrar Encuentro
                    </button>

                </div>

                <div class="box-body">
                  <table class="table table-bordered table-striped dt-responsive tablas">
                    
                     <thead>

                       <tr>
                        <th style="width: 10px">#</th>
                        <th>Encuentros</th>
                        <th>Acciones</th>
                      </tr>

                    </thead>

                    <tbody>
                      <?php

                        $listaE = ControladorEncuentro::ctrMostrarEncuentro();

                        rsort($listaE["detalle"]);
                        foreach ($listaE["detalle"] as $key => $value) {
                          echo '<tr>
                                <td>'.($key+1).'</td>';
                                $date = date_create($value["fecha"]);
                          echo '<td>'.date_format($date, 'd-m-Y').'</td>

                                <td>

                                  <div class="btn-group">
                                      
                                    <button class="btn btn-warning btnEditarEncuentro" idEncuentro="'.$value["id_encuentro"].'" data-toggle="modal" data-target="#modalEditarEncuentro"><i class="fa fa-pencil"></i></button>

                                    <button class="btn btn-danger btnEliminarEncuentro" idEncuentro="'.$value["id_encuentro"].'"><i class="fa fa-times"></i></button>

                                  </div>  

                                </td>

                              </tr>';
                        }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
          </div>
        </div>
        <div class="col-md-6 col-xs-12">
          <div class="box box-warning">
            
            <div class="box-header with-border"></div>
              <div class="box">
                <div class="box-header with-border form-group">
                  <div class="input-group">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalCargarOfrenda">
                      Registrar Ofrenda
                    </button>
                  </div>
                </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped dt-responsive tablas">
                    
                     <thead>

                       <tr>
                        <th style="width: 10px">#</th>
                        <th>Fechas</th>
                        <th>Montos</th>
                        <th>Clases</th>
                        <th>Acciones</th>
                      </tr>

                    </thead>
                    <tbody>
                      <?php

                        $listaO = ControladorOfrenda::ctrMostrarOfrenda("",0);
                        $listC = ControladorClase::ctrMostrarClase(0);
                        rsort($listaO["detalle"]);
                        foreach ($listaO["detalle"] as $key => $value) {
                          echo '<tr>
                                <td>'.($key+1).'</td>';
                                $date = date_create($value["fecha_recaudado"]);
                          echo '<td>'.date_format($date, 'd-m-Y').'</td>

                                <td>'.$value["monto"].'</td>';


                          foreach ($listC["detalle"] as $key => $valueClase) {
                            if($valueClase["id_clase"] == $value["id_clase"]){
                              echo '<td>'.$valueClase["nombre"].'</td>';
                              break;
                            }
                                  
                          }
                          echo '<td>

                                  <div class="btn-group">
                                      
                                    <button class="btn btn-warning btnEditarOfrenda" idOfrenda="'.$value["id_ofrenda"].'" data-toggle="modal" data-target="#modalEditarOfrenda"><i class="fa fa-pencil"></i></button>

                                    <button class="btn btn-danger btnEliminarOfrenda" idOfrenda="'.$value["id_ofrenda"].'"><i class="fa fa-times"></i></button>

                                  </div> 

                                </td>

                              </tr>';
                        }
                      ?>
                    </tbody>
                  </table>

                </div>
              </div>
          </div>
        </div>
    </div>

  </section>
 
</div>

<!--=====================================
MODAL ENCUENTRO
======================================-->

<div id="modalCargarEncuentro" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post" enctype="multipart/form-data">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Encuentro</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">
           <!-- ENTRADA PARA LA FECHA -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="nuevaFecha" name="nuevaFecha" placeholder="" required>

              </div>

            </div>

            <div class="form-group">
              <div class="input-group"><b>Observacion</b>
                <textarea id="observacion" name="observacion" rows="5" cols="75px"></textarea>
              </div>
            </div>

          </div>
        </div>
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar</button>

        </div>

        <?php

          $crearEncuentro = new ControladorEncuentro();
          $crearEncuentro -> ctrCrearEncuentro();
        ?>

      </form>

    </div>

  </div>

</div>

<!--=====================================
MODAL EDITAR ENCUENTRO
======================================-->

<div id="modalEditarEncuentro" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post" enctype="multipart/form-data">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Editar Encuentro</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA EL ID -->
            
            <div class="form-group">
              
              <div class="input-group">
                
                <input type="text" class="form-control input-lg" id="editarId" name="editarId" placeholder="Id"style="display: none;"  readonly required>

              </div>

            </div>

           <!-- ENTRADA PARA LA FECHA -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="editarFechaEncuentro" name="editarFechaEncuentro" placeholder="" required>

              </div>

            </div>

            <div class="form-group"><b>Observacion</b>
              <div class="input-group">
                <textarea id="editarObservacion" name="editarObservacion" rows="5" cols="55"></textarea>
              </div>
            </div>

          </div>
        </div>
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar</button>

        </div>

        <?php
          $editarEncuentro = new ControladorEncuentro();
          $editarEncuentro -> ctrEditarEncuentro();
        ?>

      </form>

    </div>

  </div>

</div>

<!--=====================================
MODAL OFRENDA
======================================-->

<div id="modalCargarOfrenda" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Ofrenda</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">
           <!-- ENTRADA PARA SELECCIONAR CLASE -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="nuevaClase" name="nuevaClase" required>
                  
                  <option value="">SELECCIONAR CLASE</option>

                  <?php

                  $item = null;
                  $valor = null;
                  $orden = "nombre";

                  $clase = ControladorClase::ctrMostrarClase(0);

                  foreach ($clase["detalle"] as $key => $value) {
                    
                    echo '<option value="'.$value["id_clase"].'">'.$value["nombre"].'</option>';
                  }

                  ?>
  
                </select>
              </div>
            </div>

           <!-- ENTRADA PARA LA FECHA -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="nuevaFechaOfrenda" name="nuevaFechaOfrenda" placeholder="" required>

              </div>

              </div>
             <!-- Entrada de la ofrenda -->

              <div class="col-xs-6" style="padding-left:0px">

                <div class="input-group">

                  <span class="input-group-addon"><b>C</b><i class="ion ion-social-usd"></i></span>
                     
                  <input type="number" min="0" max="9999" step="0.01" class="form-control" id="nuevoMonto" name="nuevoMonto" placeholder="0000.00" required>
     
                </div>
                 
              </div> 

          </div>
        </div>
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar</button>

        </div>


        <?php
          $crearOfrenda = new ControladorOfrenda();
          $crearOfrenda -> ctrCrearOfrenda();
        ?>
      </form>

    </div>

  </div>

</div>

<!--=====================================
MODAL EDITAR OFRENDA
======================================-->

<div id="modalEditarOfrenda" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Editar Ofrenda</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

           <!-- ENTRADA PARA EL ID -->
            
            <div class="form-group">
              
              <div class="input-group">
                
                <input type="text" class="form-control input-lg" id="editarIdOfrenda" name="editarIdOfrenda" placeholder="Id"style="display: none;"  readonly required>

              </div>

            </div>

           <!-- ENTRADA PARA SELECCIONAR CLASE -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="editarClase" name="editarClase" required>
                  
                  <option value="">SELECCIONAR CLASE</option>

                  <?php

                  $item = null;
                  $valor = null;
                  $orden = "nombre";

                  $clase = ControladorClase::ctrMostrarClase(0);

                  foreach ($clase["detalle"] as $key => $value) {
                    
                    echo '<option value="'.$value["id_clase"].'">'.$value["nombre"].'</option>';
                  }

                  ?>
  
                </select>
              </div>
            </div>

           <!-- ENTRADA PARA LA FECHA -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="editarFechaOfrenda" name="editarFechaOfrenda" placeholder="" required>

              </div>

              </div>
             <!-- Entrada de la ofrenda -->

              <div class="col-xs-6" style="padding-left:0px">

                <div class="input-group">

                  <span class="input-group-addon"><b>C</b><i class="ion ion-social-usd"></i></span>
                     
                  <input type="number" min="0" max="9999" step="0.01" class="form-control" id="editarMonto" name="editarMonto" placeholder="0000.00" required>
     
                </div>
                 
              </div> 

          </div>
        </div>
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Modificar</button>

        </div>


        <?php 
          $editarOfrenda = new ControladorOfrenda();
          $editarOfrenda -> ctrEditarOfrenda();
        ?>
      </form>

    </div>

  </div>

</div>

<?php
  $borrarEncuentro = new ControladorEncuentro();
  $borrarEncuentro -> ctrBorrarEncuentro();
?>

<?php
  $borrarOfrenda = new ControladorOfrenda();
  $borrarOfrenda -> ctrBorrarOfrenda();
?>
