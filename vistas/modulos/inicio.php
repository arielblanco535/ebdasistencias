<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Graficos
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Tablero</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">
      <div class="box-header with-border">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalCargarDash">
            Formato
          </button>

         <?php
              if(isset($_POST["nuevaClase"])){
               echo '<button style="margin-left: 10px" type="button" class="btn btn-success btnImprimirFormato" claseId="'.$_POST["nuevaClase"].'" anios="'.$_POST["nuevoAnio"].'">
                  Imprimir Asistencia
                </button>';
              }
            ?>
      </div>

     <div class="row">
      <div class="box-body">
        <div class="col-md-12 col-xs-12">


        </div>

        <div class="col-md-6 col-xs-12">


        </div>

         <div class="col-lg-6">


        </div>

         <div class="col-lg-6">
           
          <!--<?php

          if($_SESSION["perfil"] =="Administrador" || $_SESSION["perfil"] =="Maestro"){

             echo '<div class="box box-success">

             <div class="box-header">

             <h1>Bienvenid@ ' .$_SESSION["nombre"].'</h1>

             </div>

             </div>';

          }

          ?>-->

         </div>
        </div>
     </div>
    </div>
  </section>
 
</div>

<!--=====================================
MODAL CARGAR DASH
======================================-->

<div id="modalCargarDash" class="modal fade formato" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">DashBoard</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">
           <!-- ENTRADA PARA SELECCIONAR CLASE -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="nuevaClase" name="nuevaClase" required>
                  
                  <option value="">SELECCIONAR CLASE</option>

                  <?php

                  $item = null;
                  $valor = null;
                  $orden = "nombre";

                  $clase = ControladorClase::ctrMostrarClase(0);

                  foreach ($clase["detalle"] as $key => $value) {
                    
                    echo '<option value="'.$value["id_clase"].'">'.$value["nombre"].'</option>';
                  }

                  ?>
  
                </select>
              </div>
            </div>


           <!-- ENTRADA PARA SELECCIONAR AÑO -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="nuevoAnio" name="nuevoAnio" required>
                  
                  <option value="">SELECCIONAR AÑO</option>

                  <?php

                  $anio = ControladorPeriodo::ctrMostrarPeriodo();

                  foreach ($anio["detalle"] as $key => $value) {
                    
                     echo '<option value="'.$value["anio"].'" anio="'.$value["anio"].'">'.$value["anio"].'</option>';
                  }

                  ?>
  
                </select>
              </div>
            </div>

          </div>
        </div>
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary" anio="$_POST['nuevoAnio']">Cargar Lista</button>

        </div>


      </form>

    </div>

  </div>

</div>
