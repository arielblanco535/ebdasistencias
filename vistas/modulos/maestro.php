<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Maestro
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Maestro</li>

    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
 
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalRegistrarMaestro">
          
          Registrar Maestro

        </button>
        
      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Nombres</th>
           <th>Apellidos</th>
           <th>Fecha Nacimiento</th>
           <th>Cedula</th>
           <th>Estado</th>
           <th>Titular</th>
           <th>Clase</th>
           <th>Acciones</th>

         </tr> 

        </thead>

        <tbody>

         <?php

            $listM = ControladorMaestro::ctrMostrarMaestro(0);
            $listC = ControladorClase::ctrMostrarClase(0);
            foreach ($listM["detalle"] as $key => $value) {
           
              echo ' <tr>

                    <td>'.($key+1).'</td>

                    <td>'.$value["nombre_maestro"].'</td>

                    <td>'.$value["apellido_maestro"].'</td>';

                    $date = date_create($value["fecha_nacimiento"]);
                    echo '<td>'.date_format($date, 'd-m-Y').'</td>

                    <td>'.$value["cedula"].'</td>';
                    
                  if($value["estado"] != 0){

                    echo '<td><button class="btn btn-success btn-xs btnActivarMaestro" idMaestro="'.$value["id_maestro"].'" idClase="'.$value["id_clase"].'" estadoMaestro="0">Activado</button></td>';

                  }else{

                    echo '<td><button class="btn btn-danger btn-xs btnActivarMaestro" idMaestro="'.$value["id_maestro"].'" idClase="'.$value["id_clase"].'" estadoMaestro="1">Desactivado</button></td>';

                  }             
                  if($value["titular"] != 0){

                    echo '<td><button class="btn btn-success btn-xs btnTitularMaestro" idMaestro="'.$value["id_maestro"].'" idClase="'.$value["id_clase"].'" titularMaestro="0">SI</button></td>';

                  }else{

                    echo '<td><button class="btn btn-danger btn-xs btnTitularMaestro" idMaestro="'.$value["id_maestro"].'" idClase="'.$value["id_clase"].'" titularMaestro="1">NO</button></td>';

                  }

                  foreach ($listC["detalle"] as $key => $valueClase) {
                    if($valueClase["id_clase"] == $value["id_clase"]){
                      echo '<td>'.$valueClase["nombre"].'</td>';
                      break;
                    }
                          
                  }

                  echo '<td>

                      <div class="btn-group">
                          
                        <button class="btn btn-warning btnEditarMaestro" idMaestro="'.$value["id_maestro"].'" data-toggle="modal" data-target="#modalEditarMaestro"><i class="fa fa-pencil"></i></button>

                        <button class="btn btn-danger btnEliminarMaestro" idMaestro="'.$value["id_maestro"].'"><i class="fa fa-times"></i></button>

                      </div> 

                    </td>

                      
                  </tr>';
            }
        ?>
        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>

<!--=====================================
MODAL REGISTRAR MAESTRO
======================================-->
<div id="modalRegistrarMaestro" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Registrar Maestro</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA EL NOMBRE -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="nuevoNombreMaestro" name="nuevoNombreMaestro" placeholder="Ingresar nombre" required>

              </div>

            </div>

            <!-- ENTRADA PARA EL APELLIDO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="nuevoApellidoMaestro" name="nuevoApellidoMaestro" placeholder="Ingresar apellido" required>

              </div>

            </div>

            <!-- ENTRADA PARA LA FECHA NACIMIENTO-->
            
            <div class="form-group"><b>Fecha de Nacimiento</b>
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="nuevaFechaNacimiento" name="nuevaFechaNacimiento" placeholder="" required>

              </div>

            </div>

            <!-- ENTRADA PARA EL CEDULA -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-id-card"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="nuevaCedula" name="nuevaCedula" placeholder="Ingresar cedula" required>

              </div>

            </div>

           <!-- ENTRADA PARA SELECCIONAR CLASE -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="nuevaClase" name="nuevaClase" required>
                  
                  <option value="">SELECCIONAR CLASE</option>

                  <?php

                  $item = null;
                  $valor = null;
                  $orden = "nombre";

                  $clase = ControladorClase::ctrMostrarClase(0);

                  foreach ($clase["detalle"] as $key => $value) {
                    
                    echo '<option value="'.$value["id_clase"].'">'.$value["nombre"].'</option>';
                  }

                  ?>
  
                </select>
              </div>
            </div>

          </div>
        </div>
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Registrar Maestro</button>

        </div>

        <?php
          $crearMaestro = new ControladorMaestro();
          $crearMaestro -> ctrCrearMaestro();
        ?>

      </form>

    </div>

  </div>

</div>

<!--=====================================
MODAL EDITAR MAESTRO
======================================-->
<div id="modalEditarMaestro" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Modificar Maestro</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA EL ID -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <input type="text" class="form-control input-lg" name="editarIdMaestro" id="editarIdMaestro" style="display: none;" readonly required>

              </div>
            </div>

            <!-- ENTRADA PARA EL NOMBRE -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="editarNombreMaestro" name="editarNombreMaestro" placeholder="Ingresar nombre" required>

              </div>

            </div>

            <!-- ENTRADA PARA EL APELLIDO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="editarApellidoMaestro" name="editarApellidoMaestro" placeholder="Ingresar apellido" required>

              </div>

            </div>

            <!-- ENTRADA PARA LA FECHA NACIMIENTO-->
            
            <div class="form-group"><b>Fecha de Nacimiento</b>
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="editarFechaNacimiento" name="editarFechaNacimiento" placeholder="" required>

              </div>

            </div>

            <!-- ENTRADA PARA EL CEDULA -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-id-card"></i></span> 

                <input type="text" style="text-transform: uppercase;" class="form-control input-lg" id="editarCedula" name="editarCedula" placeholder="Ingresar cedula" required>

              </div>

            </div>

           <!-- ENTRADA PARA SELECCIONAR CLASE -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="editarClase" name="editarClase" required>
                  
                  <option value="">SELECCIONAR CLASE</option>

                  <?php

                  $item = null;
                  $valor = null;
                  $orden = "nombre";

                  $clase = ControladorClase::ctrMostrarClase(0);

                  foreach ($clase["detalle"] as $key => $value) {
                    
                    echo '<option value="'.$value["id_clase"].'">'.$value["nombre"].'</option>';
                  }

                  ?>
  
                </select>
              </div>
            </div>

          </div>
        </div>
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Registrar Maestro</button>

        </div>

        <?php
          $editarMaestro = new ControladorMaestro();
          $editarMaestro -> ctrEditarMaestro();
        ?>

      </form>

    </div>

  </div>

</div>

<?php
  $borrarMaestro = new ControladorMaestro();
  $borrarMaestro -> ctrBorrarMaestro();
?>