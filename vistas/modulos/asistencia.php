<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Asistencia
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Asistencias</li>

    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
 
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAsistencia">
          
          Cargar Asistencia

        </button>
        
      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Codigo</th>
           <th>Nombre</th>
           <th>Apellido</th>
           <th>Presente</th>

         </tr> 

        </thead>

        <tbody>

          <?php
            if(isset($_POST["asistenciaClase"])){

              $ofrenda = ControladorOfrenda::ctrMostrarOfrenda($_POST["asistenciaFecha"], $_POST["asistenciaClase"]);

              echo "<b>Ofrenda: ".$ofrenda."</b>";
              $listac = ControladorMatricula::ctrMostrarMatricula();
              $listas = ControladorAsistencia::ctrMostrarAsistencia();
              $encu = ControladorEncuentro::ctrMostrarEncuentro();
              $enct = "";
              foreach ($encu["detalle"] as $key => $valueE) {
                if($valueE["fecha"] == $_POST["asistenciaFecha"])
                  $enct = $valueE["id_encuentro"];
              }
              $i = 0;
              foreach ($listas["detalle"] as $key => $value) {
                if($value["fecha"] == $_POST["asistenciaFecha"] && $_POST["asistenciaPeriodo"] == $value["id_periodo"] && $_POST["asistenciaClase"] == $value["id_clase"]){
                  $i = 1;
                  break;
                }
              }
              if($i == 0){
              $control = new ControladorAsistencia(); 
              $control -> ctrGuardar($_POST["asistenciaFecha"]);
              }
              foreach ($listac["detalle"] as $key => $value) {
                if($value["estado"] == "1" && $_POST["asistenciaPeriodo"] == $value["id_periodo"] && $_POST["asistenciaClase"] == $value["id_clase"]){

                echo ' <tr>

                      <td>'.($key+1).'</td>

                      <td>'.$value["codigo_alumno"].'</td>

                      <td>'.$value["nombre_alumno"].'</td>

                      <td>'.$value["apellido_alumno"].'</td>';
                        $it = 100;
                      foreach ($listas["detalle"] as $key => $valueA) {
                        if($valueA["estado"] == 1 && $valueA["fecha"] == $_POST["asistenciaFecha"] && $valueA["id_clase"] == 1 && $valueA["id_matricula"] == $value["id_matricula"]){
                          if($valueA["presente"] == 1){

                            echo '<td><button class="btn btn-success btn-xs btnAsistencia" idAsistencia="'.$valueA["id_asistencia"].'" estadoAsistencia="0" idEncuentro="'.$valueA["id_encuentro"].'" idMatricula="'.$valueA["id_matricula"].'">P</button></td>';
                            $it = 1;
                          }else if($valueA["presente"] == 0){

                            echo '<td><button class="btn btn-danger btn-xs btnAsistencia" idAsistencia="'.$valueA["id_asistencia"].'" estadoAsistencia="1" idEncuentro="'.$valueA["id_encuentro"].'" idMatricula="'.$valueA["id_matricula"].'">A</button></td>';
                            $it = 1;
                          }

                        }
                      }
                      if($it == 100){
                        echo '<td><button class="btn btn-warnig btn-xs btnAsistencia" idAsistencia="0" estadoAsistencia="1" idEncuentro="'.$enct.'" idMatricula="'.$value["id_matricula"].'">A</button></td>';
                      }
                      echo '</tr>';
                  }
              }
            }
          ?>
        </tbody>

       </table>

      <div>
        
      </div>

      <div>
      </div>

      </div>

    </div>

  </section>

</div>

<!--=====================================
MODAL REGISTRAR ALUMNO
======================================-->

<div id="modalAsistencia" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Cargar Asistencia</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

           <!-- ENTRADA PARA SELECCIONAR CLASE -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="asistenciaClase" name="asistenciaClase" required>
                  
                  <option value="">SELECCIONAR CLASE</option>

                  <?php

                  $item = null;
                  $valor = null;
                  $orden = "nombre";

                  $clase = ControladorClase::ctrMostrarClase(0);

                  foreach ($clase["detalle"] as $key => $value) {
                    
                    echo '<option value="'.$value["id_clase"].'">'.$value["nombre"].'</option>';
                  }

                  ?>
  
                </select>
              </div>
            </div>

            <!-- ENTRADA PARA LA FECHA ENCUENTRO-->
            
            <div class="form-group"><b>Fecha de Encuentro</b>
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class='fa fa-calendar'></i></span> 

                <input type="date" class="form-control input-lg" id="asistenciaFecha" name="asistenciaFecha" placeholder="" required>

              </div>

            </div>

           <!-- ENTRADA PARA SELECCIONAR PERIODO -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="glyphicon glyphicon-menu-hamburger"></i></span> 

                <select class="form-control input-lg" id="asistenciaPeriodo" name="asistenciaPeriodo" required>
                  
                  <option value="">SELECCIONAR PERIODO</option>
                  <?php

                  $orden = "nombre";

                  $periodo = ControladorPeriodo::ctrMostrarPeriodo();

                  foreach ($periodo["detalle"] as $key => $value) {
                    
                    echo '<option value="'.$value["id_periodo"].'">'.$value["anio"].'</option>';
                  }

                  ?>
  
                </select>
              </div>
            </div>


          </div>
        </div>
        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Cargar</button>

        </div>

      </form>

    </div>

  </div>

</div>


<?php
  $borrarAlumno = new ControladorAlumno();
  $borrarAlumno -> ctrBorrarAlumno();
?>
