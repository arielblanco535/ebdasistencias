/*=============================================
MATRICULA ALUMNO
=============================================*/
$(".tablas").on("click", ".btnMatriculaAlumno", function(){

	var idAlumno = $(this).attr("idAlumno");
	
	var datos = new FormData();
	datos.append("idAlumno", idAlumno);

	$.ajax({

		url:"ajax/alumnos.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){
			
			$("#matriculaIdAlumno").val(respuesta["id_alumno"]);
			$("#matriculaCodigoAlumno").val(respuesta["codigo_alumno"]);
			$("#matriculaNombreAlumno").val(respuesta["nombre_alumno"]);
			$("#matriculaApellidoAlumno").val(respuesta["apellido_alumno"]);
			$("#matriculaFechaNacimientoAlumno").val(respuesta["fecha_nacimiento"]);
			//$("#matriculaFechaRegistroAlumno").val(respuesta["fecha_ingreso"]);
		}

	});

})

/*=============================================
EDITAR MATRICULA ALUMNO
=============================================*/
$(".tablas").on("click", ".btnEditarMatricula", function(){

	var idMatricula = $(this).attr("idMatricula");
	
	var datos = new FormData();
	datos.append("idMatricula", idMatricula);

	$.ajax({

		url:"ajax/matriculas.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){
			
			$("#editarMatriculaId").val(respuesta["id_matricula"]);
			$("#editarMatriculaCodigo").val(respuesta["codigo_alumno"]);
			$("#editarMatriculaNombre").val(respuesta["nombre_alumno"]);
			$("#editarMatriculaApellido").val(respuesta["apellido_alumno"]);
			$("#editarMatriculaFechaNacimiento").val(respuesta["fecha_nacimiento"]);
			//$("#matriculaFechaRegistroAlumno").val(respuesta["fecha_ingreso"]);
		}

	});

	var matriculaId = 293;
	
	var datos = new FormData();
	datos.append("matriculaId", matriculaId);

	$.ajax({

		url:"ajax/matriculas.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){
			
			$("#editarMatriculaFechaRegistro").val(respuesta["fecha"]);
		}

	});
})
