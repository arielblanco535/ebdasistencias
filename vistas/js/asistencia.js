/*=============================================
ACTIVAR MAESTRO
=============================================*/
$(".tablas").on("click", ".btnAsistencia", function(){

	var idAsistencia = $(this).attr("idAsistencia");
  var estadoAsistencia = $(this).attr("estadoAsistencia");
  var idEncuentro = $(this).attr("idEncuentro");
  var idMatricula = $(this).attr("idMatricula");
  var id = 0;

	var datos = new FormData();
  datos.append("idAsistencia", idAsistencia);
  datos.append("estadoAsistencia", estadoAsistencia);
  datos.append("idEncuentro", idEncuentro);
  datos.append("idMatricula", idMatricula);

  	$.ajax({

	  url:"ajax/asistencias.ajax.php",
	  method: "POST",
	  data: datos,
	  cache: false,
      contentType: false,
      processData: false,
      dataType: "json",
      success: function(respuesta){
            if(window.matchMedia("(max-width:767px)").matches && respuesta["result"] != "stop"){

      		 swal({
  			      title: "Asistencia actualizada",
  			      type: "success",
  			      confirmButtonText: "¡Cerrar!"
    		    }).then(function(result) {
			        if (result.value) {

			        	window.location = "asistencia";

			        }
    				});
        	} 
      }

  	})

  	if(estadoAsistencia == 0){

  		$(this).removeClass('btn-success');
  		$(this).addClass('btn-danger');
  		$(this).html('A');
  		$(this).attr('estadoAsistencia',1);

  	}else{

  		$(this).addClass('btn-success');
  		$(this).removeClass('btn-danger');
  		$(this).html('P');
  		$(this).attr('estadoAsistencia',0);

  	}

})
