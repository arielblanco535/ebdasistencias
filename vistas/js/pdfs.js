/*=============================================
IMPRIMIR LISTADO DE CLASE 
=============================================*/
$(".btnImprimirLista").click(function(){

	var idClase = $(this).attr("claseId");
	var estado = $(this).attr("state");
	var anio = $(this).attr("anios");

	window.open("extensiones/tcpdf/pdf/listadoclase.php?clase="+idClase+"&estado="+estado+"&anio="+anio, "_blank");
})

/*=============================================
IMPRIMIR ASISTENCIA DOMINICAL
=============================================*/
$(".btnImprimirAsistencia").click(function(){

	var idClase = $(this).attr("claseId");
	var fecha = $(this).attr("fecha");

	window.open("extensiones/tcpdf/pdf/asistenciadominical.php?clase="+idClase+"&fecha="+fecha, "_blank");
})

/*=============================================
IMPRIMIR ASISTENCIA MENSUAL
=============================================*/
$(".btnImprimirAsistenciaMensual").click(function(){

	var idClase = $(this).attr("claseId");
	var mes = $(this).attr("mes");
	var anio = $(this).attr("anio");

	window.open("extensiones/tcpdf/pdf/asistenciamensual.php?clase="+idClase+"&mes="+mes+"&anio="+anio, "_blank");
})

/*=============================================
IMPRIMIR LISTA DE PORCENTAJE
=============================================*/
$(".btnImprimirPorcentaje").click(function(){

	var idClase = $(this).attr("claseId");
	var mesI = $(this).attr("mesI");
	var mesF = $(this).attr("mesF");
	var anio = $(this).attr("anio");

	window.open("extensiones/tcpdf/pdf/asistenciaporcentaje.php?clase="+idClase+"&mesI="+mesI+"&mesF="+mesF+"&anio="+anio, "_blank");
})

/*=============================================
IMPRIMIR FORMATO
=============================================*/
$(".btnImprimirFormato").click(function(){

	var idClase = $(this).attr("claseId");
	var anio = $(this).attr("anios");

	window.open("extensiones/tcpdf/pdf/formato.php?clase="+idClase+"&estado="+1+"&anio="+anio, "_blank");
})

/*=============================================
IMPRIMIR FORMATO
=============================================*/
$(".btnImprimirFormatoTodos").click(function(){

	var idClase = $(this).attr("claseId");
	var anio = $(this).attr("anio");

	window.open("extensiones/tcpdf/pdf/formato.php?clase="+idClase+"&estado="+1+"&anio="+anio, "_blank");
})
