/*=============================================
EDITAR ENCUENTRO
=============================================*/
$(".tablas").on("click", ".btnEditarEncuentro", function(){

	var idEncuentro = $(this).attr("idEncuentro");
	
	var datos = new FormData();
	datos.append("idEncuentro", idEncuentro);

	$.ajax({

		url:"ajax/encuentros.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){
			
			$("#editarId").val(respuesta["id_encuentro"]);
			$("#editarFechaEncuentro").val(respuesta["fecha"]);
			$("#editarObservacion").val(respuesta["Observacion"]);
		}

	});

})

/*=============================================
ELIMINAR ENCUENTRO
=============================================*/
$(".tablas").on("click", ".btnEliminarEncuentro", function(){

  var idEncuentro = $(this).attr("idEncuentro");

  swal({
    title: '¿Está seguro de borrar el encuentro?',
    text: "¡Si no lo está puede cancelar la accíón!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si, borrar encuentro!'
  }).then(function(result){

    if(result.value){

      window.location = "index.php?ruta=encuentroofrenda&idEncuentro="+idEncuentro;

    }

  })

})
