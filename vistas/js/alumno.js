/*=============================================
EDITAR ALUMNO
=============================================*/
$(".tablas").on("click", ".btnEditarAlumno", function(){

	var idAlumno = $(this).attr("idAlumno");
	
	var datos = new FormData();
	datos.append("idAlumno", idAlumno);

	$.ajax({

		url:"ajax/alumnos.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){
			
			$("#editarIdAlumno").val(respuesta["id_alumno"]);
			$("#editarCodigo").val(respuesta["codigo_alumno"]);
			$("#editarNombreAlumno").val(respuesta["nombre_alumno"]);
			$("#editarApellidoAlumno").val(respuesta["apellido_alumno"]);
			$("#editarFechaNac").val(respuesta["fecha_nacimiento"]);
			$("#editarFechaIng").val(respuesta["fecha_ingreso"]);
		}

	});

})

/*=============================================
ELIMINAR ALUMNO
=============================================*/
$(".tablas").on("click", ".btnEliminarAlumno", function(){

  var idAlumno = $(this).attr("idAlumno");

  swal({
    title: '¿Está seguro de borrar al Alumno?',
    text: "¡Si no lo está puede cancelar la accíón!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si, borrar Alumno!'
  }).then(function(result){

    if(result.value){

      window.location = "index.php?ruta=alumno&idAlumno="+idAlumno;

    }

  })

})
