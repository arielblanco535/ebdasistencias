/*=============================================
EDITAR MAESTRO
=============================================*/
$(".tablas").on("click", ".btnEditarMaestro", function(){

  var idMaestro = $(this).attr("idMaestro");
  
  var datos = new FormData();
  datos.append("idMaestro", idMaestro);

  $.ajax({

    url:"ajax/maestros.ajax.php",
    method: "POST",
    data: datos,
    cache: false,
    contentType: false,
    processData: false,
    dataType: "json",
    success: function(respuesta){
      
      $("#editarIdMaestro").val(respuesta["id_maestro"]);
      $("#editarNombreMaestro").val(respuesta["nombre_maestro"]);
      $("#editarApellidoMaestro").val(respuesta["apellido_maestro"]);
      $("#editarFechaNacimiento").val(respuesta["fecha_nacimiento"]);
      $("#editarCedula").val(respuesta["cedula"]);
    }

  });

})

/*=============================================
ACTIVAR MAESTRO
=============================================*/
$(".tablas").on("click", ".btnActivarMaestro", function(){

	var idMaestro = $(this).attr("idMaestro");
	var estadoMaestro = $(this).attr("estadoMaestro");
  var idClase = $(this).attr("idClase");

	var datos = new FormData();
 	datos.append("activarId", idMaestro);
  datos.append("activarMaestro", estadoMaestro);
  datos.append("idClase", idClase);

  	$.ajax({

	  url:"ajax/maestros.ajax.php",
	  method: "POST",
	  data: datos,
	  cache: false,
      contentType: false,
      processData: false,
      dataType: "json",
      success: function(respuesta){

        if(respuesta["result"] == "stop"){
          swal({
            title: "No se puede desactivar un maestro titular",
            type: "warning",
            confirmButtonText: "¡Cerrar!"
          }).then(function(result) {
              if (result.value) {

                window.location = "maestro";

              }
          });
        }

      		if(window.matchMedia("(max-width:767px)").matches && respuesta["result"] != "stop"){

      		 swal({
  			      title: "El maestro ha sido actualizado",
  			      type: "success",
  			      confirmButtonText: "¡Cerrar!"
    		    }).then(function(result) {
			        if (result.value) {

			        	window.location = "maestro";

			        }
    				});
        	} else {
          swal({
            title: "No se puede desactivar un maestro titular",
            type: "warning",
            confirmButtonText: "¡Cerrar!"
          }).then(function(result) {
              if (result.value) {

                window.location = "maestro";

              }
          });
        }

      }

  	})

  	if(estadoMaestro == 0){

  		$(this).removeClass('btn-success');
  		$(this).addClass('btn-danger');
  		$(this).html('Desactivado');
  		$(this).attr('estadoMaestro',1);

  	}else{

  		$(this).addClass('btn-success');
  		$(this).removeClass('btn-danger');
  		$(this).html('Activado');
  		$(this).attr('estadoMaestro',0);

  	}

})

/*=============================================
TITULAR MAESTRO
=============================================*/
$(".tablas").on("click", ".btnTitularMaestro", function(){

  var idMaestro = $(this).attr("idMaestro");
  var titularMaestro = $(this).attr("titularMaestro");
  var idClase = $(this).attr("idClase");

  var datos = new FormData();
  datos.append("titularId", idMaestro);
    datos.append("titularMaestro", titularMaestro);
    datos.append("idClase", idClase);

    $.ajax({

    url:"ajax/maestros.ajax.php",
    method: "POST",
    data: datos,
    cache: false,
      contentType: false,
      processData: false,
      dataType: "json",
      success: function(respuesta){

        if(respuesta["result"] == "error"){

          swal({
            title: "El maestro titular no puede estar desactivado",
            type: "error",
            confirmButtonText: "¡Cerrar!"
          }).then(function(result) {
              if (result.value) {

                window.location = "maestro";

              }

          });
        } else if(respuesta["result"] == "stop"){
          swal({
            title: "La clase ya tiene un maestro titular",
            type: "warning",
            confirmButtonText: "¡Cerrar!"
          }).then(function(result) {
              if (result.value) {

                window.location = "maestro";

              }

          });
        } 

        if(window.matchMedia("(max-width:767px)").matches && respuesta["result"] == "error"){

          swal({
            title: "El maestro titular no puede estar desactivado",
            type: "error",
            confirmButtonText: "¡Cerrar!"
          }).then(function(result) {
              if (result.value) {

                window.location = "maestro";

              }
          });
        } else if(window.matchMedia("(max-width:767px)").matches && respuesta["result"] == "stop"){

          swal({
            title: "La clase ya tiene un maestro titular",
            type: "warning",
            confirmButtonText: "¡Cerrar!"
          }).then(function(result) {
              if (result.value) {

                window.location = "maestro";

              }
          });
        }else if(window.matchMedia("(max-width:767px)").matches){
          
         swal({
            title: "El maestro ha sido actualizado",
            type: "success",
            confirmButtonText: "¡Cerrar!"
          }).then(function(result) {
            if (result.value) {

              window.location = "maestro";

            }
          });
        }

      }

    })

    if(titularMaestro == 0){

      $(this).removeClass('btn-success');
      $(this).addClass('btn-danger');
      $(this).html('NO');
      $(this).attr('titularMaestro',1);

    }else{

      $(this).addClass('btn-success');
      $(this).removeClass('btn-danger');
      $(this).html('SI');
      $(this).attr('titularMaestro',0);

    }

})

/*=============================================
ELIMINAR ALUMNO
=============================================*/
$(".tablas").on("click", ".btnEliminarMaestro", function(){

  var idMaestro = $(this).attr("idMaestro");

  swal({
    title: '¿Está seguro de borrar al Maestro?',
    text: "¡Si no lo está puede cancelar la accíón!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si, borrar Maestro!'
  }).then(function(result){

    if(result.value){

      window.location = "index.php?ruta=maestro&idMaestro="+idMaestro;

    }

  })

})
