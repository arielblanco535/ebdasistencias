/*=============================================
EDITAR OFRENDA
=============================================*/
$(".tablas").on("click", ".btnEditarOfrenda", function(){

	var idOfrenda = $(this).attr("idOfrenda");
	
	var datos = new FormData();
	datos.append("idOfrenda", idOfrenda);

	$.ajax({

		url:"ajax/ofrendas.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){
			
			$("#editarIdOfrenda").val(respuesta["id_ofrenda"]);
			$("#editarFechaOfrenda").val(respuesta["fecha_recaudado"]);
			$("#editarMonto").val(respuesta["monto"]);
		}

	});

})

/*=============================================
ELIMINAR OFRENDA
=============================================*/
$(".tablas").on("click", ".btnEliminarOfrenda", function(){

  var idOfrenda = $(this).attr("idOfrenda");

  swal({
    title: '¿Está seguro de borrar la ofrenda?',
    text: "¡Si no lo está puede cancelar la accíón!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si, borrar Ofrenda!'
  }).then(function(result){

    if(result.value){

      window.location = "index.php?ruta=encuentroofrenda&idOfrenda="+idOfrenda;

    }

  })

})
